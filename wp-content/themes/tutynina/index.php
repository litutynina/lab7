<?php get_header( 'header.php'); ?>
	<div class="fh5co-loader"></div>

	<aside id="fh5co-aside" role="sidebar" class="text-center" style="background-image: url(<?php the_field('img', '2'); ?>);">
		<h1 id="fh5co-logo"><a href="index.html"><?php the_field('ref', '2'); ?></a></h1>
	</aside>

	<div id="fh5co-main-content">
		<div class="dt js-dt">
			<div class="dtc js-dtc">
			

				<div class="row">
					<div class="col-md-12">
						<div class="row">
							<div class="col-lg-7">
								<div class="fh5co-intro animate-box">
									<h2><?php the_field('main_title', '2'); ?></h2>
									<p><?php the_field('text', '2'); ?></p>
								</div>
							</div>
							
						
						</div>
					</div>
				</div>
					
			</div>
		</div>

		<div id="fh5co-footer">
			<div class="row">
				
				<div class="col-md-6 fh5co-copyright">
					<p><?php the_field('text1', '2'); ?></p>
				</div>
			</div>
		</div>
		
	</div>
	<?php get_footer('footer.php'); ?>
	

